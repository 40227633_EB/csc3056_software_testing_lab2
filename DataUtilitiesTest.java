package org.jfree.data.test;

import org.jfree.data.DataUtilities;
import org.jfree.data.DefaultKeyedValues2D;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.jfree.data.DefaultKeyedValues2D;
import org.jfree.data.Values2D;

import junit.framework.TestCase;

public class DataUtilitiesTest extends TestCase {

	private Values2D values2D;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	protected void setUp() throws Exception {
		super.setUp();
		DefaultKeyedValues2D testValues = new DefaultKeyedValues2D(); 
	    values2D = testValues; 
	    testValues.addValue(1, 0, 0); 
	    testValues.addValue(4, 1, 0);
	}

	@After
	protected void tearDown() throws Exception {
		super.tearDown();
		values2D=null;
	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testNullDataColumnTotal() 
	{ 
	  try 
	  { 
	    DataUtilities.calculateColumnTotal(null, 0); 
	    fail("No exception thrown Expected outcome was: a thrown exception  of type: InvalidParameterException"); 
	  } 
	  catch (Exception e) 
	  { 
	    assertTrue("Incorrect exception type thrown",  e.getClass().equals(InvalidParameterException.class)); 
	  } 
	} 

}
